from django.contrib import admin
from .models import SalesPerson, PotentialCustomer, Sales, AutomobileVO

admin.site.register(SalesPerson)
admin.site.register(PotentialCustomer)
admin.site.register(Sales)
admin.site.register(AutomobileVO)
