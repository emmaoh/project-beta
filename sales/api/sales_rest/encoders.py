from common.json import ModelEncoder, DecimalEncoder
from .models import Sales, SalesPerson, PotentialCustomer, AutomobileVO


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "employee_name",
        "employee_number",
    ]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "id",
        "customer_name",
        "customer_address",
        "customer_phone_number",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_href",
        "vin",
    ]


class SalesEncoder(ModelEncoder, DecimalEncoder):
    model = Sales
    properties = [
        "id",
        "automobile",
        "sales_person",
        "potential_customer",
        "sales_price",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "potential_customer": PotentialCustomerEncoder(),
    }
