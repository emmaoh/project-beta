import { Carousel } from 'react-bootstrap';
import car1 from './images/car1.jpg'
import car2 from './images/car2.jpg'
import car3 from './images/car3.jpg'

function CarouselFade() {
  return (
    <Carousel fade className='carousel' interval={5000}>
      <Carousel.Item style={{ zIndex: -1 }}>
        <Carousel.Caption>
          <div className="text-center">
            <h1 className="display-5 fw-bold text-on-image">LIFT AUTO</h1>
            <h4 className="text-on-image">Everything you need to have to manage automobile sales and services</h4>
          </div>
        </Carousel.Caption>
        <img
          className="d-block"
          style={{ overflow: "hidden", height: "600px", width: "100%", lineHeight: "1", maxWidth: "100vw" }}
          src={car1}
          alt="car1"
        />
      </Carousel.Item>

      <Carousel.Item style={{ zIndex: -1 }}>
        <Carousel.Caption>
          <div className="text-center">
            <h1 className="display-5 fw-bold text-on-image">LIFT AUTO</h1>
            <h4 className="text-on-image">Everything you need to have to manage automobile sales and services</h4>
          </div>
        </Carousel.Caption>
        <img
          className="d-block"
          style={{ overflow: "hidden", height: "600px", width: "100%", lineHeight: "1", maxWidth: "100vw" }}
          src={car2}
          alt="car2"
        />
      </Carousel.Item>

      <Carousel.Item style={{ zIndex: -1 }}>
        <Carousel.Caption>
          <div className="text-center">
            <h1 className="display-5 fw-bold text-on-image">LIFT AUTO</h1>
            <h4 className="text-on-image">Everything you need to have to manage automobile sales and services</h4>
          </div>
        </Carousel.Caption>
        <img
          className="d-block w-100"
          style={{ overflow: "hidden", height: "600px", width: "100%", lineHeight: "1", maxWidth: "100vw" }}
          src={car3}
          alt="car3"
        />
      </Carousel.Item>
    </Carousel>
  );
}

export default CarouselFade;