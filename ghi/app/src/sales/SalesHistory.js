import React from 'react'

class SalesHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales: [],
            salesPersons: [],
            salesPerson: '',
        };
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this)
        this.handleSalesChange = this.handleSalesChange.bind(this);
        this.onSearch = this.onSearch.bind(this)
    }

    handleSalesPersonChange(event) {
        const value = event.target.value
        this.setState({ salesPerson: value })
    }

    handleSalesChange(event) {
        const value = event.target.value
        this.setState({ sales: value })
    }

    async onSearch(event) {
        event.preventDefault();
    }



    async componentDidMount() {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales })

            const salesPersonsUrl = 'http://localhost:8090/api/sales_persons/';
            const salesPersonsResponse = await fetch(salesPersonsUrl);
            if (salesPersonsResponse.ok) {
                const salesPersonsData = await salesPersonsResponse.json();
                this.setState({ salesPersons: salesPersonsData.sales_persons })
            }
        }
    }

    render() {
        return (
            <>
                <br />
                <h1>Sales person history</h1>
                <br />
                <form onSubmit={this.onSearch} id='select-salesperson-form'>
                    <select onChange={this.handleSalesPersonChange} value={this.state.salesPerson} name="sales_person" id="sales_person" className='form-select' required>
                        <option value="">Choose a Sales Person</option>
                        {this.state.salesPersons.map(salesPerson => {
                            return (
                                <option key={salesPerson.id} value={salesPerson.id}>
                                    {salesPerson.employee_name}
                                </option>
                            );
                        })}
                    </select>
                </form>
                <p></p>
                <div className="appointment-list">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Sales person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.sales.map(sale => {
                                if (sale.sales_person.id == this.state.salesPerson) {
                                    return (
                                        <tr key={sale.id}>
                                            <td>{sale.sales_person.employee_name}</td>
                                            <td>{sale.potential_customer.customer_name}</td>
                                            <td>{sale.automobile.vin}</td>
                                            <td>$ {new Intl.NumberFormat().format(sale.sales_price)}</td>
                                        </tr>
                                    )
                                }
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}

export default SalesHistory