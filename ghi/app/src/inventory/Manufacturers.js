import React from 'react'
import '../index.css'

function Manufacturers(props) {
    return (
        <>
            <p></p>
            <h2>Manufacturer</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                <td>{manufacturer.name}</td>
                                <td style={{ textAlign: "right" }}>
                                    <button className="cancel" onClick={() => props.ManufacturerOnDelete(manufacturer)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );

}

export default Manufacturers;